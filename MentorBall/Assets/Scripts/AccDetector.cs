﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class AccDetector : MonoBehaviour {

	public float speed = 1.0F;
	private bool isTouchedFlag = false;
	private bool isTurnOver = false;
	private Vector3 totalVectorChange;
	private Vector3 startVector;
	public Text DebugText;
	private bool throwInit = false;

	void Update()
	{
		//Debug.Log("Input : " + Input.acceleration);
		if (throwInit && !isTurnOver && !isTouchedFlag)
		{
			isTouchedFlag = true;
			startVector = Input.acceleration;
		}

		if (Input.GetMouseButtonUp(0) && isTouchedFlag)
		{
			isTouchedFlag = false;
			isTurnOver = true;

			//totalVectorChange = Input.acceleration - startVector;
			/*var newThrow = FindThrowAngle();
			DebugText.text = "ThrowInfos : \n" + newThrow.ToString();*/
		}

		DetectThrow();



	}

	public void ThrowInit()
	{
		throwInit = true;
	}



	void DetectThrow()
	{
		if (isTouchedFlag)
		{
			var change = Input.acceleration - startVector;
			totalVectorChange += change;
			DebugText.text = "READY";
		}
	}
/*
	Throw FindThrowAngle()
	{
		Throw newThrow = new Throw();
		if (totalVectorChange.z > 20 )
		{
			newThrow.direction = ThrowDirection.Forward;
			newThrow.speed = totalVectorChange.z-20;
		}
		else
		{
			if (totalVectorChange.x > 5)
			{
				newThrow.direction = ThrowDirection.Left;
				newThrow.speed = totalVectorChange.x - 5;
			}

			else if (totalVectorChange.x < -5)
			{
				newThrow.direction = ThrowDirection.Right;
				newThrow.speed = totalVectorChange.x + 5;
			}
		}
		return newThrow;

	}*/
}
