﻿using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    public NameManager namePanel;
    public PositionManager positionPanel;
    //todo: change
    public GameplayPanelManager gamePanel;
    public Base_Manager splashPanel;
    public Base_Manager scorePanel;

    public List<Base_Manager> panelList;

    private static PanelManager _instance = null;

    // Game Instance Singleton
    public static PanelManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);

        panelList.Add(namePanel);
        panelList.Add(positionPanel);
        panelList.Add(gamePanel);
        panelList.Add(splashPanel);
        panelList.Add(scorePanel);
        CloseAllPanels();
    }

    private void Start()
    {
        CloseAllPanels();
        splashPanel.gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        GameClient.RoundStarted += RoundStarted;
        GameClient.RoundFinished += RoundFinished;
    }
  
    private void OnDisable()
    {
        GameClient.RoundStarted -= RoundStarted;
        GameClient.RoundFinished -= RoundFinished;
    }

    private void RoundFinished(Dictionary<string, int> dictionary)
    {

    }

    private void RoundStarted(int connectionId)
    {
	    var isBallActive = Client.Instance.GameClient.ConnectionId == connectionId;

        GoToGamePlay(isBallActive);
    }

    public void GoToNamePanel ()
	{
	    CloseAllPanels();
	    namePanel.gameObject.SetActive(true);
    }

    public void GoToGamePlay(bool isBallActive)
    {
	    CloseAllPanels();
		gamePanel.Open(isBallActive);               
    }

    public void GoToPositionPanel()
    {
        CloseAllPanels();
        positionPanel.gameObject.SetActive(true);
    }

    public void GoToScorePanel()
    {
        CloseAllPanels();
        scorePanel.gameObject.SetActive(true);
    }

    void CloseAllPanels()
    {
        foreach (Base_Manager item in panelList)
        {
            item.ClosePanel();
        }
    }
}
