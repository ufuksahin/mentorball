﻿using System;
using System.Collections.Generic;
using Server;
using Server.Models;
using UnityEngine.Networking;

public partial class GameClient
{
    public Dictionary<int, PlayerPeer> PlayerPeers = new Dictionary<int, PlayerPeer>();

    public static Action<Dictionary<int, PlayerPeer>> RoomDataReceived;
    public static Action<PlayerPeer> APlayerJoined;
    public static Action<bool, int> Joined;
    public static Action<int> RoundStarted; 
    public static Action<Dictionary<string, int>> RoundFinished; 
    public static Action ItsYourTurn;
    public int ConnectionId;

    protected void RegisterGameOperations()
    {
        NetworkClient.RegisterHandler(ClientGameOps.GetRoom, OnGotRoom);
        NetworkClient.RegisterHandler(ClientGameOps.Join, OnJoined);
        NetworkClient.RegisterHandler(ClientGameOps.PlayerJoinedEvent, OnPlayerJoined);
        NetworkClient.RegisterHandler(ClientGameOps.StartRound, OnStartRound);
        NetworkClient.RegisterHandler(ClientGameOps.ThrowBallEvent, OnThrowBallEvent);
        NetworkClient.RegisterHandler(ClientGameOps.ReceiveBallEvent, OnBallReceived);
        NetworkClient.RegisterHandler(ClientGameOps.CatchBallResultEvent, OnCatchBallReceived);
        NetworkClient.RegisterHandler(ClientGameOps.FinishRound, OnFinishRoundReceived);
    }
    
    public void GetRoom()
    {
        Log.Debug("GetRoom");
        NetworkClient.Send(ClientGameOps.GetRoom, new BasicRequest());
    }

    private void OnGotRoom(NetworkMessage netmsg)
    {
        Log.Debug("OnGotRoom");
        var response = netmsg.ReadMessage<GetRoomResponse>();
        PlayerPeers = response.Players;

        if (RoomDataReceived != null) RoomDataReceived(PlayerPeers);
    }

	public void Join(string name, int position)
	{
		Log.Debug("Game server authentication");
		NetworkClient.Send(ClientGameOps.Join, new JoinRequest(name, position));
	}

	private void OnJoined(NetworkMessage netmsg)
    {
        var operation = netmsg.ReadMessage<OperationResponse<IntResponse>>();
        Log.Debug("Game server authentication: " + operation.Result);

        if (operation.Result != OpResult.Success)
        {
            Log.Error("Slot is already occupied!");
            if (Joined != null) Joined(false, 0);
            return;
        }

        ConnectionId = operation.Response.Value;

        if (Joined != null) Joined(true, operation.Response.Value);
    }

    private void OnPlayerJoined(NetworkMessage netmsg)
    {
        var playerJoinedEvent = netmsg.ReadMessage<PlayerJoinedEvent>();

        if (!PlayerPeers.ContainsKey(playerJoinedEvent.PlayerPeer.ConnectionId))
        {
            PlayerPeers.Add(playerJoinedEvent.PlayerPeer.ConnectionId, playerJoinedEvent.PlayerPeer);
        }

        if (APlayerJoined != null) APlayerJoined(playerJoinedEvent.PlayerPeer);
    }

    private void OnStartRound(NetworkMessage netmsg)
    {
        var startRoundEvent = netmsg.ReadMessage<StartRoundEvent>();
        //OTHER PLAYERS POSITIONS RECEIVED!!

        if (RoundStarted != null) RoundStarted(startRoundEvent.ConnectionId);
    }

    private void OnThrowBallEvent(NetworkMessage netmsg)
    {
        //TODO: Throw ball
        if (ItsYourTurn != null) ItsYourTurn();
    }

    public void SendBall(MovementDirection movementDirection, float speed)
    {
        Log.Debug("Send ball");
        NetworkClient.Send(ClientGameOps.SendBall, new SendBallRequest(movementDirection, speed));
    }

	public void CatchBall(int senderId, bool result)
	{
		Log.Debug("Catch ball");

		NetworkClient.Send(ClientGameOps.CatchBall, new CatchBallRequest(senderId, result));
	}

	public int LastSenderId;

	private void OnBallReceived(NetworkMessage netmsg)
    {
        Log.Debug("Ball received");

        ReceiveBallEvent receiveBallEvent = netmsg.ReadMessage<ReceiveBallEvent>();

	    LastSenderId = receiveBallEvent.SenderId;

		GameManager.Instance._BallBehaviour.BallRecieved(receiveBallEvent.MovementDirection,receiveBallEvent.Speed);

    }

    private void OnCatchBallReceived(NetworkMessage netmsg)
    {
        Log.Debug("Ball received");

        CatchBallResultEvent catchBallResultEvent = netmsg.ReadMessage<CatchBallResultEvent>();
        
        //TODO: Winner id set!
    }

    private void OnFinishRoundReceived(NetworkMessage netmsg)
    {
        Log.Debug("Ball received");

        FinishRoundEvent finishRoundEvent = netmsg.ReadMessage<FinishRoundEvent>();

        PanelManager.Instance.GoToScorePanel();

        if (RoundFinished != null) RoundFinished(finishRoundEvent.Scores);
    }    

}