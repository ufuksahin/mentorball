﻿using System.Collections;
using System.Collections.Generic;
using Server.Models;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
	private bool _isBallStartMoving;
	private MovementDirection _direction;
	private Vector2 _movementVector;
	private RectTransform _transform;
	private bool _isCatced;

	void Awake()
	{
		_transform = GetComponent<RectTransform>();
	}
	public void ResetBall()
	{
		_isBallStartMoving = false;
		_isCatced = false;
		_transform.anchoredPosition = new Vector2(1000000,1000000);
	}

	public void ShowBall()
	{
		_transform.anchoredPosition = Vector2.zero;
	}

	public void BallRecieved(MovementDirection direction,float speed)
	{
		_isBallStartMoving = true;
		_direction = direction;
		
		float randomness = Random.Range(-10f, +10f);
		switch (direction)
		{
			case MovementDirection.Left:
				_transform.anchoredPosition = new Vector2(-15+randomness, -10);
				_movementVector = new Vector2(speed, speed);
				break;
			case MovementDirection.Right:
				_transform.anchoredPosition = new Vector2(Screen.width + 15 + randomness, -10);
				_movementVector = new Vector2(-speed, speed);
				break;
			case MovementDirection.Forward:
				_transform.anchoredPosition = new Vector2(randomness, -10);
				_movementVector = new Vector2(0, speed);
				break;
		}
		
	}

	bool CheckIfFailed()
	{
		if (_transform.anchoredPosition.x > Screen.width + 10)
		{
			CathcFailed();
			return true;
		}

		if (_transform.anchoredPosition.y > Screen.height + 10)
		{
			CathcFailed();
			return true;
		}
		return false;
	}

	public void Catched()
	{
		_isCatced = true;
		GameManager.Instance.CatchBall();
		ResetBall();
	}

	void Update()
	{
		if (_isBallStartMoving && _isCatced &&!CheckIfFailed() )
		{
			_transform.Translate(_movementVector);
		}
	}
	

	void CathcFailed()
	{
		GameManager.Instance.CatchBallFail();
		ResetBall();
	}
}
