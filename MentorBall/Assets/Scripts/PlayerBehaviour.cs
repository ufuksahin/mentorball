﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
	public SwipeDetector Detector;

	void Awake()
	{
		Detector = GetComponent<SwipeDetector>();
		
	}

	public void StartMyTurn()
	{
		Detector.InitSwipeDetection();
	}

	public void NotMyTurn()
	{
		Detector.TurnOver();
	}

}
