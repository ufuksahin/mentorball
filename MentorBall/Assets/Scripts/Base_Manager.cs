﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base_Manager : MonoBehaviour {

    public virtual void ClosePanel()
    {
        gameObject.SetActive(false);
    }
}
