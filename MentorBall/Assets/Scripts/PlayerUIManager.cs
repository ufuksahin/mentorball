﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour
{

    public Text nameText;
    public Text scoreText;



    void GameStarted ()
    {
        SetPlayerName();

    }

    void SetPlayerName()
    {
        nameText.text = "";
    }

    void SetScore()
    {
        scoreText.text = "";
    }
}
