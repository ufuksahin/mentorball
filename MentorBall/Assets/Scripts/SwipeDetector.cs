﻿using System.Collections;
using System.Collections.Generic;
using Server.Models;
using UnityEngine;
using UnityEngine.UI;


public class SwipeDetector : MonoBehaviour
{

    private const int mMessageWidth = 200;
    private const int mMessageHeight = 64;

    private readonly Vector2 mXAxis = new Vector2(1, 0);
    private readonly Vector2 mYAxis = new Vector2(0, 1);

	private bool isTurnOver = false;
	private bool isThrowing = false;
	private float velocity;
	public Text DebugText;

	private readonly string[] mMessage = {
        "",
        "Swipe Left",
        "Swipe Right",
        "Swipe Top",
        "Swipe Bottom"
    };

    private int mMessageIndex = 0;

    // The angle range for detecting swipe
    private const float mAngleRange = 30;

    // To recognize as swipe user should at lease swipe for this many pixels
    private const float mMinSwipeDist = 50.0f;

    // To recognize as a swipe the velocity of the swipe
    // should be at least mMinVelocity
    // Reduce or increase to control the swipe speed
    private const float mMinVelocity = 200.0f;

    private Vector2 mStartPosition;
    private float mSwipeStartTime;
	private PlayerBehaviour Behaviour;
	
    // Use this for initialization
    void Awake()
    {
	    Behaviour = GetComponent<PlayerBehaviour>();
		//DebugText.text = "Ready";
	}

	public void InitSwipeDetection()
	{
		isTurnOver = false;
	}

	public void TurnOver()
	{
		isTurnOver = true;
	}

	// Update is called once per frame
	void Update()
    {

        // Mouse button down, possible chance for a swipe
        if (Input.GetMouseButtonDown(0) && !isTurnOver)
        {
	        isThrowing = true;
			// Record start time and position
			mStartPosition = new Vector2(Input.mousePosition.x,
                Input.mousePosition.y);
            mSwipeStartTime = Time.time;
            
        }

        // Mouse button up, possible chance for a swipe
        if (Input.GetMouseButtonUp(0) && isThrowing)
        {
	        isTurnOver = true;
	        isThrowing = false;
			float deltaTime = Time.time - mSwipeStartTime;

            Vector2 endPosition = new Vector2(Input.mousePosition.x,
                Input.mousePosition.y);
            Vector2 swipeVector = endPosition - mStartPosition;

            velocity = swipeVector.magnitude / deltaTime;

            if (velocity > mMinVelocity &&
                swipeVector.magnitude > mMinSwipeDist)
            {
                // if the swipe has enough velocity and enough distance

                swipeVector.Normalize();

                float angleOfSwipe = Vector2.Dot(swipeVector, mXAxis);
                angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;

                // Detect left and right swipe
                if (angleOfSwipe < mAngleRange)
                {
                    OnSwipeRight();
                }
                else if ((180.0f - angleOfSwipe) < mAngleRange)
                {
                    OnSwipeLeft();
                }
                else
                {
                    // Detect top and bottom swipe
                    angleOfSwipe = Vector2.Dot(swipeVector, mYAxis);
                    angleOfSwipe = Mathf.Acos(angleOfSwipe) * Mathf.Rad2Deg;
                    if (angleOfSwipe < mAngleRange)
                    {
                        OnSwipeTop();
                    }
                    else if ((180.0f - angleOfSwipe) < mAngleRange)
                    {
                        OnSwipeBottom();
                    }
                    else
                    {
                        mMessageIndex = 0;
                    }
                }
            }
			//Debug.Log(mMessage[mMessageIndex]  + "\nVelocity : " + velocity.ToString());
			//DebugText.text = "ThrowInfos : \n" + mMessage[mMessageIndex] + "\nVelocity : " + velocity.ToString();
		}
    }

	public void Reset()
	{
		isThrowing = false;

		//DebugText.text = "Ready";
	}


	private void OnSwipeLeft()
    {
		GameManager.Instance.ThrowBall(MovementDirection.Left, velocity/100.0f);
        mMessageIndex = 1;
	}

    private void OnSwipeRight()
    {
	    GameManager.Instance.ThrowBall(MovementDirection.Right, velocity/100.0f);
		mMessageIndex = 2;
    }

    private void OnSwipeTop()
    {
	    GameManager.Instance.ThrowBall(MovementDirection.Forward, velocity/100.0f);
		mMessageIndex = 3;
    }

    private void OnSwipeBottom()
    {
        mMessageIndex = 4;
    }
}


public enum ThrowDirection
{
	Forward,
	Left,
	Right,
	None
}

