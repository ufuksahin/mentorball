﻿using Server;
using UnityEngine.Networking;

/// <summary>
/// Game server client for game client
/// </summary>
public partial class GameClient : BaseClient
{
    public override void ConnectToServer()
    {
        base.ConnectToServer();

        RegisterGameOperations();
    }

    protected override void OnConnectToServer(NetworkMessage netMsg)
    {
        base.OnConnectToServer(netMsg);

        Log.Debug("Client is connected to Game Server");

        GetRoom();
    }

}