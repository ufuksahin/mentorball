﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Server;
using UnityEngine;
using UnityEngine.UI;

public class PositionManager : Base_Manager
{
    public List<PositionSelect> positionSelectList;

   
    private void OnEnable()
    {
        GameClient.RoomDataReceived += RoomDataReceived;
        GameClient.APlayerJoined += APlayerJoined;
        GameClient.RoundStarted += RoundStarted;        

        Client.Instance.GameClient.GetRoom();

     
    }

    private void RoundStarted(int connectionId)
    {
       //todo: start match
    }

    private void OnDisable()
    {
        GameClient.RoomDataReceived -= RoomDataReceived;
        GameClient.APlayerJoined -= APlayerJoined;         
        GameClient.RoundStarted -= RoundStarted;
    }

   

    private void RoomDataReceived(Dictionary<int, PlayerPeer> playerPeers)
    {
	    foreach (var playerPeer in playerPeers)
	    {
		    InsertPlayer(positionSelectList[playerPeer.Value.Position],playerPeer.Value);
	    }
	    
        //foreach (PositionSelect positionSelect in positionSelectList)
        //{
        //    var peer = playerPeers.FirstOrDefault(x => x.Key == positionSelect.positionIndex);
        //    if(!peer.Equals(default(KeyValuePair<int, PlayerPeer>)))
        //    {
        //        InsertPlayer(positionSelect,peer.Value);
        //    }
        //    else
        //    {
        //        positionSelect.button.interactable = true;
        //    }
        //}
    }

    private void APlayerJoined(PlayerPeer playerPeer)
    {

        InsertPlayer(positionSelectList[playerPeer.Position], playerPeer);

        
    }

    private void InsertPlayer(PositionSelect positionSelect, PlayerPeer playerPeer)
    {
        if (playerPeer.ConnectionId == Client.Instance.GameClient.ConnectionId)
        {
            foreach (var positionSelectObj in positionSelectList)
            {
                positionSelectObj.button.interactable = false;
            }
        }
        else
        {
            positionSelect.button.interactable = false;
        }
        
        positionSelect.SetPlayerName(playerPeer.Name);
    }

}
