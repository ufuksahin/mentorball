﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionSelect : MonoBehaviour {	
    public int positionIndex;
    public Text playerNameText;

    public Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        button.interactable = true;
	    playerNameText.text = "";
    }

    public	void SelectPosition (int posInt) {
		Debug.Log("Selected Position "+ posInt);
        GameClient.Joined += Joined;
        Client.Instance.GameClient.Join(playerNameText.text,posInt);
        button.interactable = false;
    }

    public void SetPlayerName(string _playerName)
    {
        playerNameText.text = _playerName;
    }

    private void Joined(bool joined, int connectionId)
    {
        GameClient.Joined -= Joined;
    }
}
