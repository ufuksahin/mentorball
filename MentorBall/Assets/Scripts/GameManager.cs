﻿using System;
using System.Collections;
using System.Collections.Generic;
using Server;
using Server.Models;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public GameClient Client;
	public PlayerBehaviour PlayerBehaviour;
	public BallBehaviour _BallBehaviour;
 
	public static GameManager Instance;

	
	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public void NotMyTurn()
	{
		PlayerBehaviour.NotMyTurn();
		_BallBehaviour.ResetBall();

	}
	public void StartThrowingBall()
	{
		_BallBehaviour.ShowBall();
		PlayerBehaviour.StartMyTurn();
	}

	private void ItsYourTurn()
	{
		PlayerBehaviour.StartMyTurn();
	}

	public void CatchBall()
	{
		Client.CatchBall(Client.LastSenderId,true);
		StartThrowingBall();
	}

	public void CatchBallFail()
	{
		Client.CatchBall(Client.LastSenderId, false);
		StartThrowingBall();
	}
	
	public void ThrowBall(MovementDirection direction, float speed)
	{
		Client.SendBall(direction,speed);
	}

}



