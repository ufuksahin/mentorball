﻿using System.Collections.Generic;
using Server;

public class ScoreManager : Base_Manager
{
    public List<ScoreItem> Scores;

    void OnEnable()
    {
        GameClient.RoundFinished += RoundFinished;
    }

    private void RoundFinished(Dictionary<string, int> scores)
    {
        GameClient.RoundFinished -= RoundFinished;

        foreach (KeyValuePair<string, int> scorePair in scores)
        {
            for (int i = 0; i < 4; i++)
            {
                Scores[i].Initialize(scorePair.Key, scorePair.Value);
            }
        }
    }

    public void OnReplayClicked()
    {
        Client.Instance.GameClient.DisconnectFromServer();
        CoroutineController.DoAfterGivenTime(1f, () =>
        {
            Client.Instance.GameClient.ConnectToServer();
        });
    }
}
