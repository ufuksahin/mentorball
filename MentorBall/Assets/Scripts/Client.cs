﻿using UnityEngine;

public class Client : MonoBehaviour
{
    public GameClient GameClient;

	void Start ()
	{
		GameClient.ConnectToServer();
	}


    private static Client _instance = null;

    // Game Instance Singleton
    public static Client Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
}
