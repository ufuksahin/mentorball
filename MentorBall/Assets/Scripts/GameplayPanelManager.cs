﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class GameplayPanelManager : Base_Manager {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Open(bool isBallActive)
	{
		Debug.Log("OpenGPP");
		gameObject.SetActive(true);
		if (isBallActive)
			GameManager.Instance.StartThrowingBall();
		else
			GameManager.Instance.NotMyTurn();
	}
}
