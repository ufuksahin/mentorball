﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NameManager : Base_Manager
{
    public InputField inputField;
    public Button button;
    public PanelManager panelManager;

	void Start () {
	    IsButtonEnabled(false);
    }

    public void CheckName()
    {
        if (inputField.text.Length > 0)
            IsButtonEnabled(true);
        else
        {
            IsButtonEnabled(false);        
    
        }

    }


    void IsButtonEnabled(bool value)
    {
        button.interactable = value;
    }

    public void NameSubmit()
    {
        Debug.Log(inputField.text);
       panelManager.GoToPositionPanel();
    }

   
}
