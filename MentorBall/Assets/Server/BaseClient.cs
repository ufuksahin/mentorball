﻿using System;
using System.Collections.Generic;
using System.Linq;
using Panteon.PanLog;
using UnityEngine;
using UnityEngine.Networking;

namespace Server
{
	public class BaseClient : MonoBehaviour
	{
		[Header("Server Settings")]
		public string ServerIpAddress = "127.0.0.1";
		public int ServerPort = 5000;

		[Header("Troubleshooting")]
		[Tooltip("If true, when client disconnects from server for any uncontrolable reason, it will reconnect automatically")]
		public bool AutoConnectOnTrouble;

		public bool HasConnectionError
		{
			get
			{
				if (ConnectionStatus != ConnectionStatus.Connected)
				{
					Debug.LogError("Not connected to Server!");
					return true;
				}

				return false;
			}
		}
		public event Action<ConnectionStatus> ConnectionStatusChanged;

		public ConnectionStatus ConnectionStatus
		{
			get { return _connectionStatus; }
			set
			{
				if (_connectionStatus == value) return;
				_connectionStatus = value;

				if (ConnectionStatusChanged != null) ConnectionStatusChanged(_connectionStatus);
			}
		}

		public NetworkClient NetworkClient;
		private ConnectionStatus _connectionStatus = ConnectionStatus.Disconnected;

		protected PanLog Log = new PanLog(typeof(BaseClient).Name, DebugLevels.Debug);


		public virtual void ConnectToServer()
		{
			if (ConnectionStatus != ConnectionStatus.Disconnected)
			{
				Debug.LogError("Already connected");
				return;
			}

			Log.Info(string.Format("Connecting to {0}:{1}", ServerIpAddress, ServerPort));

			NetworkClient = new NetworkClient();

			// short timeouts
			//var config = new ConnectionConfig();
			//config.ConnectTimeout = 100;
			//todo: test this.
			//config.DisconnectTimeout = 2000;
			//NetworkClient.Configure(config, 1);

			//System Operations
			NetworkClient.RegisterHandler(MsgType.Connect, OnConnectToServer);
			NetworkClient.RegisterHandler(MsgType.Disconnect, OnDisconnectFromServer);
			NetworkClient.RegisterHandler(MsgType.Error, OnError);
			NetworkClient.Connect(ServerIpAddress, ServerPort);

			DontDestroyOnLoad(gameObject);
		}

		public virtual void ConnectToServer(ConnectionInfo connectionInfo)
		{
			ServerIpAddress = connectionInfo.Address;
			ServerPort = connectionInfo.Port;

			ConnectToServer();
		}

		public virtual void DisconnectFromServer()
		{
			if (NetworkClient == null)
				return;

			NetworkTransport.RemoveHost(0);

			List<short> messageTypes = NetworkClient.handlers.Keys.ToList();

			//Unregister handlers before shut down.
			foreach (short messageType in messageTypes)
			{
				NetworkClient.UnregisterHandler(messageType);
			}
			//todo: look here again.
			//NetworkClient.Disconnect();
			NetworkClient.Shutdown();
			NetworkClient = null;

			ConnectionStatus = ConnectionStatus.Disconnected;
		}

		#region System Handlers

		protected virtual void OnConnectToServer(NetworkMessage netMsg)
		{
			ConnectionStatus = ConnectionStatus.Connected;
		}

		protected virtual void OnDisconnectFromServer(NetworkMessage netMsg)
		{
			Log.Debug("OnDisconnectFromServer!");
			DisconnectFromServer();

			if (AutoConnectOnTrouble)
				CoroutineController.DoAfterGivenTime(1f, ReConnect);
		}

		protected virtual void OnError(NetworkMessage netMsg)
		{
			Log.Error("Server error!");

			ConnectionStatus = ConnectionStatus.Disconnected;

			if (AutoConnectOnTrouble)
				CoroutineController.DoAfterGivenTime(1f, ReConnect);
		}

		protected virtual void ReConnect()
		{
			Debug.LogError("Re-connecting to the server...");

			ConnectToServer();
		}

		#endregion
	}
}
