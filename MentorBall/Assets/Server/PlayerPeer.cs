﻿using UnityEngine.Networking;

namespace Server
{
    public class PlayerPeer : Peer
    {
        public string Name;
        public int Score;
        public int Position;

        public PlayerPeer() { }

        public PlayerPeer(int connectionId, string name, int score, int position) : base(connectionId)
        {
            Name = name;
            Score = score;
            Position = position;
        }

        public PlayerPeer(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Serialize(NetworkWriter writer)
        {
            base.Serialize(writer);

            writer.Write(Name);
            writer.Write(Score);
            writer.Write(Position);
        }

        public override void Deserialize(NetworkReader reader)
        {
            base.Deserialize(reader);

            Name = reader.ReadString();
            Score = reader.ReadInt32();
            Position = reader.ReadInt32();
        }
    }
}
