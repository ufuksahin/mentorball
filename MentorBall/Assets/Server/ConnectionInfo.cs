﻿using System;
using UnityEngine.Networking;

namespace Server
{
    public class ConnectionInfo : MessageBase, IEquatable<ConnectionInfo>
    {
        public string Address;
        public short Port;

        public ConnectionInfo() { }

        public ConnectionInfo(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public ConnectionInfo(string adress, short port)
        {
            Address = adress;
            Port = port;
        }

        public bool Equals(ConnectionInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(Address, other.Address) && Port == other.Port;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ConnectionInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Address != null ? Address.GetHashCode() : 0) * 397) ^ Port.GetHashCode();
            }
        }
    }
}
