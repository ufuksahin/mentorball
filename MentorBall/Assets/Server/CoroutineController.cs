﻿using System;
using System.Collections;
using UnityEngine;

namespace Server
{
    public class CoroutineController : MonoBehaviour
    {
        #region Variables

        private static CoroutineController _instance;
        public static CoroutineController Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject go = new GameObject("_CoroutineController");
                    _instance = go.AddComponent<CoroutineController>();
                    DontDestroyOnLoad(_instance);
                }

                return _instance;
            }
        }

		#endregion

		#region Coroutines
		public Coroutine StartChildCoroutine(IEnumerator coroutineMethod)
	    {
		    return StartCoroutine(coroutineMethod);
	    }

	    public void StopChildCoroutine(Coroutine coroutine)
	    {
		    if (coroutine == null)
		    {
			    Debug.LogWarning("coroutine doesn't exist!");
			    return;
		    }

		    StopCoroutine(coroutine);
	    }

		public static void DoAfterFixedUpdate(Action actionToInvoke)
        {
            Instance.StartCoroutine(_instance.Wait(Time.fixedDeltaTime, actionToInvoke));
        }

        public static void DoAfterGivenTime(float time, Action actionToInvoke)
        {
            Instance.StartCoroutine(_instance.Wait(time, actionToInvoke));
        }

        IEnumerator Wait(float time, Action actionToInvoke)
        {
            yield return new WaitForSeconds(time);

            actionToInvoke.Invoke();
        }	    
		#endregion
	}
}
