﻿using System.Collections.Generic;
using UnityEngine.Networking;

namespace Server
{
    public enum OpResult : byte
    {
        Success = 0,
        Fail = 1,
    }

    public class BasicEvent : MessageBase { }

    public class BasicRequest : MessageBase { }

    public class UserRequest : MessageBase
    {
        public long UserId;
        public int ConnectionId;

        public UserRequest() { }

        public UserRequest(long userId, int connectionId)
        {
            UserId = userId;
            ConnectionId = connectionId;
        }
    }

    public class IntRequest : MessageBase
    {
        public int Value;

        public IntRequest() { }

        public IntRequest(int value)
        {
            Value = value;
        }
    }

    public class LongRequest : MessageBase
    {
        public long Value;

        public LongRequest() { }

        public LongRequest(long value)
        {
            Value = value;
        }
    }

    public class IntResponse : MessageBase
    {
        public int Value;

        public IntResponse() { }

        public IntResponse(int value)
        {
            Value = value;
        }
    }

    public class LongResponse : MessageBase
    {
        public long Value;

        public LongResponse() { }

        public LongResponse(long value)
        {
            Value = value;
        }
    }
    
    public class IntArrayResponse : MessageBase
    {
        public List<int> Value;

        public IntArrayResponse() { }

        public IntArrayResponse(List<int> value)
        {
            Value = value;
        }

        public IntArrayResponse(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Serialize(NetworkWriter writer)
        {
            writer.Write(Value.Count);

            for (int i = 0; i < Value.Count; i++)
            {
                writer.Write(Value[i]);
            }
        }

        public override void Deserialize(NetworkReader reader)
        {
            Value = new List<int>();

            for (int i = 0; i < reader.ReadInt32(); i++)
            {
                Value.Add(reader.ReadInt32());
            }
        }
    }

    public class ByteRequest : MessageBase
    {
        public byte Value;

        public ByteRequest() { }

        public ByteRequest(byte value)
        {
            Value = value;
        }
    }
    public class ShortRequest : MessageBase
    {
        public short Value;

        public ShortRequest() { }

        public ShortRequest(short value)
        {
            Value = value;
        }
    }

    public class ByteResponse : MessageBase
    {
        public byte Value;

        public ByteResponse() { }

        public ByteResponse(byte value)
        {
            Value = value;
        }
    }

    public class ShortResponse : MessageBase
    {
        public short Value;

        public ShortResponse() { }

        public ShortResponse(short value)
        {
            Value = value;
        }
    }

    public class StringRequest : MessageBase
    {
        public string Value;

        public StringRequest() { }

        public StringRequest(string value)
        {
            Value = value;
        }
    }

    public class StringResponse : MessageBase
    {
        public string Value;

        public StringResponse() { }

        public StringResponse(string value)
        {
            Value = value;
        }
    }

    public class BasicResponse : MessageBase
    {
        public OpResult Result;

        public BasicResponse() { }

        public BasicResponse(OpResult result)
        {
            Result = result;
        }
    }
}
