﻿using System;
using System.Collections.Generic;
using System.Linq;
using Server.Models;
using UnityEngine.Networking;

namespace Server
{
    public partial class GameServer
    {
        /// <summary>
        /// CONNECTION ID, PLAYER PEER -> Account Data/
        /// </summary>
        public Dictionary<int, PlayerPeer> PlayerPeers = new Dictionary<int, PlayerPeer>();
        public List<int> ReservedSlots = new List<int>();

        private float _timer = 200f;

        protected void RegisterClientOperations()
        {
            #region Handlers
            RegisterHandler(ClientGameOps.GetRoom, OnGetRoom);
            RegisterHandler(ClientGameOps.Join, OnJoin);
            RegisterHandler(ClientGameOps.SendBall, OnSendBall);
            RegisterHandler(ClientGameOps.CatchBall, OnCatchBall);

            #endregion
        }

        private void OnGetRoom(PlayerPeer playerpeer, NetworkMessage netmsg)
        {
            Log.Debug("Get room");

            netmsg.conn.Send(ClientGameOps.GetRoom, new GetRoomResponse(PlayerPeers));
        }

        private void OnJoin(PlayerPeer playerPeer, NetworkMessage netmsg)
        {
            Log.Debug("Player is joining");

            var request = netmsg.ReadMessage<JoinRequest>();

            PlayerPeer playerOnSlot = PlayerPeers.Values.SingleOrDefault(p => p.Position == request.Position);
            if (playerOnSlot != null)
            {
                //Slot is already occupied
                netmsg.conn.Send(ClientGameOps.Join, new OperationResponse<BasicResponse>(OpResult.Fail));
                return;
            }

            playerPeer = new PlayerPeer(netmsg.conn.connectionId, request.Name, 0, request.Position);

            //Add to PlayerPeers
            PlayerPeers.Add(netmsg.conn.connectionId, playerPeer);

            netmsg.conn.Send(ClientGameOps.Join, new OperationResponse<IntResponse>(OpResult.Success, new IntResponse(playerPeer.ConnectionId)));

            SendToAll(ClientGameOps.PlayerJoinedEvent, new PlayerJoinedEvent(playerPeer));

            if (PlayerPeers.Count == 4)
            {
                StartRound();

                CoroutineController.DoAfterGivenTime(_timer, FinishRound);
            }
        }

        private void StartRound()
        {
            int chosenPlayer = UnityEngine.Random.Range(0, 4);

            SendToAll(ClientGameOps.StartRound, new StartRoundEvent(PlayerPeers[PlayerPeers.Keys.ToList()[chosenPlayer]].ConnectionId));
        }

        private void FinishRound()
        {
            var scores = new Dictionary<string, int>();

            foreach (var playerPeer in PlayerPeers)
            {
                scores.Add(playerPeer.Value.Name,playerPeer.Value.Score);
            }

            SendToAll(ClientGameOps.FinishRound, new FinishRoundEvent(scores));
        }

        private void OnSendBall(PlayerPeer playerpeer, NetworkMessage netmsg)
        {
            var request = netmsg.ReadMessage<SendBallRequest>();

            int targetPlayer = playerpeer.Position;
            MovementDirection enterDirection = MovementDirection.Forward;

            switch (request.MovementDirection)
            {
                case MovementDirection.Forward:
                    targetPlayer = (targetPlayer + 2) % 4;
                    enterDirection = MovementDirection.Forward;
                    break;
                case MovementDirection.Left:
                    targetPlayer = (targetPlayer + 1) % 4;
                    enterDirection = MovementDirection.Right;
                    break;
                case MovementDirection.Right:
                    targetPlayer = (targetPlayer + 3) % 4;
                    enterDirection = MovementDirection.Left;
                    break;
            }

            ReceiveBallEvent receiveBallEvent = new ReceiveBallEvent(playerpeer.ConnectionId, enterDirection, request.Speed);

            PlayerPeer targetPlayerPeer = PlayerPeers.Values.SingleOrDefault(p => p.Position == targetPlayer);

            if (targetPlayerPeer == null)
            {
                Log.Error("PLAYER PEER NOT FOUND!");
                return;
            }

            SendToUser(targetPlayerPeer.Position, ClientGameOps.ReceiveBallEvent, receiveBallEvent);
        }


        private void OnCatchBall(PlayerPeer playerpeer, NetworkMessage netmsg)
        {
            CatchBallRequest request = netmsg.ReadMessage<CatchBallRequest>();

            int winnerId = request.Result ? playerpeer.ConnectionId : request.SenderUserId;

            PlayerPeers[winnerId].Score += 1;

            SendToAll(ClientGameOps.CatchBallResultEvent, new CatchBallResultEvent(winnerId));
        }

        #region Util
        public PlayerPeer GetPlayerPeer(int connectionId)
        {
            if (!PlayerPeers.ContainsKey(connectionId))
            {
                Log.Error("PlayerPeer NOT FOUND! connectionId: " + connectionId);
                return null;
            }

            return PlayerPeers[connectionId];
        }

        private void OnDisconnect(NetworkMessage netmsg)
        {
            if (!PlayerPeers.ContainsKey(netmsg.conn.connectionId))
            {
                Log.Warning("Player peer NOT found!");
                return;
            }

            //Remove from peers
            PlayerPeers.Remove(netmsg.conn.connectionId);
        }
        #endregion
    }
}
