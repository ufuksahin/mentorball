﻿using System.Collections.Generic;
using UnityEngine.Networking;

namespace Server.Models
{
    public class GetRoomResponse : MessageBase
    {
        public Dictionary<int, PlayerPeer> Players;

        public GetRoomResponse() { }

        public GetRoomResponse(Dictionary<int, PlayerPeer> players)
        {
            Players = players;
        }

        public GetRoomResponse(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Serialize(NetworkWriter writer)
        {
            base.Serialize(writer);

            writer.Write(Players.Count);
            foreach (PlayerPeer playerPeer in Players.Values)
            {
                playerPeer.Serialize(writer);
            }
        }

        public override void Deserialize(NetworkReader reader)
        {
            base.Deserialize(reader);

            int playerCount = reader.ReadInt32();
            Players = new Dictionary<int, PlayerPeer>();
            for (int i = 0; i < playerCount; i++)
            {
                PlayerPeer peer = new PlayerPeer(reader);
                Players.Add(peer.ConnectionId, peer);
            }
        }
    }
}
