﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class PlayerJoinedEvent : MessageBase
    {
        public PlayerPeer PlayerPeer;

        public PlayerJoinedEvent(PlayerPeer playerPeer)
        {
            PlayerPeer = playerPeer;
        }

        public PlayerJoinedEvent() { }

        public PlayerJoinedEvent(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Serialize(NetworkWriter writer)
        {
            base.Serialize(writer);
            PlayerPeer.Serialize(writer);
        }

        public override void Deserialize(NetworkReader reader)
        {
            base.Deserialize(reader);
            PlayerPeer = new PlayerPeer(reader);
        }
    }
}
