﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class CatchBallRequest : MessageBase
    {
        public int SenderUserId;
        public bool Result;

        public CatchBallRequest() { }

        public CatchBallRequest(int senderUserId, bool result)
        {
            SenderUserId = senderUserId;
            Result = result;
        }

        public CatchBallRequest(NetworkReader reader)
        {
            Deserialize(reader);
        }
    }
}
