﻿namespace Server.Models
{
    public enum MovementDirection : byte
    {
        Left = 1,
        Right = 2,
        Forward = 3
    }
}
