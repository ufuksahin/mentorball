﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class JoinRequest : MessageBase
    {
        public string Name;
        public int Position;

        public JoinRequest()
        {
            
        }

        public JoinRequest(string name, int position)
        {
            Name = name;
            Position = position;
        }

        public JoinRequest(NetworkReader reader)
        {
            Deserialize(reader);
        }
    }
}
