﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class CatchBallResultEvent : MessageBase
    {
        public int WinnerId;

        public CatchBallResultEvent()
        {
        }

        public CatchBallResultEvent(int winnerId)
        {
            WinnerId = winnerId;
        }

        public CatchBallResultEvent(NetworkReader reader)
        {
            Deserialize(reader);
        }
    }
}
