﻿using System.Collections.Generic;
using UnityEngine.Networking;

namespace Server.Models
{
    public class FinishRoundEvent : MessageBase
    {
        public Dictionary<string, int> Scores;

        public FinishRoundEvent() { }

        public FinishRoundEvent(Dictionary<string, int> scores)
        {
            Scores = scores;
        }

        public FinishRoundEvent(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Deserialize(NetworkReader reader)
        {
            base.Deserialize(reader);

            Scores = new Dictionary<string, int>();
            byte count = reader.ReadByte();
            for (int i = 0; i < count; i++)
            {
                Scores.Add(reader.ReadString(),reader.ReadInt32());
            }
        }

        public override void Serialize(NetworkWriter writer)
        {
            base.Serialize(writer);

            writer.Write((byte)Scores.Count);
            foreach (KeyValuePair<string, int> keyValuePair in Scores)
            {
                writer.Write(keyValuePair.Key);
                writer.Write(keyValuePair.Value);
            }
        }
    }
}
