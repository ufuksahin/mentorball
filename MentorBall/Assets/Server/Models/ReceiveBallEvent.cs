﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class ReceiveBallEvent : MessageBase
    {
	    public int SenderId;
        public MovementDirection MovementDirection;
        public float Speed;

        public ReceiveBallEvent() { }

        public ReceiveBallEvent(int senderId, MovementDirection movementDirection, float speed)
        {
	        SenderId = senderId;
            MovementDirection = movementDirection;
            Speed = speed;
        }

        public ReceiveBallEvent(NetworkReader reader)
        {
            Deserialize(reader);
        }
    }
}
