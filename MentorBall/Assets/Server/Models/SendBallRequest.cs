﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class SendBallRequest : MessageBase
    {
        public MovementDirection MovementDirection;
        public float Speed;

        public SendBallRequest() { }

        public SendBallRequest(MovementDirection movementDirection, float speed)
        {
            MovementDirection = movementDirection;
            Speed = speed;
        }

        public SendBallRequest(NetworkReader reader)
        {
            Deserialize(reader);
        }
    }
}
