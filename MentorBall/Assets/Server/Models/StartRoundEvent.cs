﻿using UnityEngine.Networking;

namespace Server.Models
{
    public class StartRoundEvent : MessageBase
    {
        public int ConnectionId;

        public StartRoundEvent() { }

        public StartRoundEvent(int connectionId)
        {
            ConnectionId = connectionId;
        }

        public StartRoundEvent(NetworkReader reader)
        {
            Deserialize(reader);
        }
    }
}
