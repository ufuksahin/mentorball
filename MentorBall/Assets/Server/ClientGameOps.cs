﻿namespace Server
{
    public class ClientGameOps
    {
        public const short GetRoom = 80;
        public const short Join = 90;
        public const short PlayerJoinedEvent = 91;
        public const short StartRound = 100;
        public const short ThrowBallEvent = 101; 
        public const short SendBall = 102; 
        public const short ReceiveBallEvent = 103; 
        public const short CatchBall = 104; 
        public const short CatchBallResultEvent = 105; 
        public const short FinishRound = 106; 
    }
}
