﻿using UnityEngine.Networking;

namespace Server
{
    public class OperationResponse<TMessageBase> : MessageBase where TMessageBase : MessageBase, new()
    {
        public OpResult Result;
        public TMessageBase Response;

        public OperationResponse() { }

        public OperationResponse(OpResult result, TMessageBase response = null)
        {
            Result = result;
            Response = response;
        }

        public OperationResponse(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Serialize(NetworkWriter writer)
        {
            writer.Write((byte)Result);

            writer.Write(Response == null);

            if (Response != null)
                Response.Serialize(writer);
        }

        public override void Deserialize(NetworkReader reader)
        {
            Result = (OpResult)reader.ReadByte();

            if (!reader.ReadBoolean())
            {
                Response = new TMessageBase();
                Response.Deserialize(reader);
            }
        }
    }
}
