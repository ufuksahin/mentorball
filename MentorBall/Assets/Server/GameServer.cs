using System.Collections.Generic;
using Panteon.PanLog;
using UnityEngine.Networking;

namespace Server
{
    public partial class GameServer : BaseServer
    {
        public new PanLog Log = new PanLog(typeof(GameServer).Name, DebugLevels.Debug);

        public override void StartServer()
        {
            Log.Debug("Starting game server on port: 5000");

            base.StartServer();

            //Server Operations
            RegisterClientOperations();
        }

        public override void StopServer()
        {
            base.StopServer();
        }

        protected override void OnClientDisconnect(NetworkMessage netMsg)
        {
            base.OnClientDisconnect(netMsg);

            OnDisconnect(netMsg);
        }

        /// <summary>
        /// Send message to peer by connectionId.
        /// Returns FALSE if peer is NULL
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="opCode"></param>
        /// <param name="message"></param>
        public bool SendToUser(int connectionId, short opCode, MessageBase message)
        {
            Peer gamePlayerPeer = GetPlayerPeer(connectionId);

            if (gamePlayerPeer == null)
            {
                Log.Debug("PlayerPeer is NULL");
                return false;
            }

            NetworkServer.SendToClient(gamePlayerPeer.ConnectionId, opCode, message);

            return true;
        }

        #region Message Handling
        public delegate void MessageDelegate(PlayerPeer playerPeer, NetworkMessage netmsg);

        public Dictionary<short, MessageDelegate> Handlers = new Dictionary<short, MessageDelegate>();

        public void RegisterHandler(short messageType, MessageDelegate handler)
        {
            if (Handlers.ContainsKey(messageType)) return;

            Handlers.Add(messageType, handler);

            NetworkServer.RegisterHandler(messageType, HandleMessage);
        }

        public virtual void HandleMessage(NetworkMessage networkMessage)
        {
            if (!Handlers.ContainsKey(networkMessage.msgType))
            {
                Log.Error("No Handler Found! MessageType: " + networkMessage.msgType);
                return;
            }

            PlayerPeer playerPeer = null;
            if (PlayerPeers.ContainsKey(networkMessage.conn.connectionId))
            {
                playerPeer = PlayerPeers[networkMessage.conn.connectionId];
            }

            //Invoke handler
            Handlers[networkMessage.msgType].Invoke(playerPeer, networkMessage);
        }
        #endregion
    }
}
