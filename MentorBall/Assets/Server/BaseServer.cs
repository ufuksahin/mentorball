﻿using System;
using System.Collections.Generic;
using Panteon.PanLog;
using UnityEngine;
using UnityEngine.Networking;

namespace Server
{
    public enum ServerStatus
    {
        Stopped = 0,
        Running = 1
    }

    public class BaseServer : MonoBehaviour
    {
        public event Action<ServerStatus> ServerStatusChanged;

        public ServerStatus ServerStatus
        {
            get { return _serverStatus; }
            set
            {
                if (_serverStatus == value) return;

                _serverStatus = value;

                if (ServerStatusChanged != null) ServerStatusChanged(_serverStatus);
            }
        }

        [Header("Running in Editor")]
        [Tooltip("If true, when running in editor, server will start automatically")]
        public bool AutoStartInEditor;

        public PanLog Log = new PanLog(typeof(BaseServer).Name, DebugLevels.Debug);

        private ServerStatus _serverStatus;

        protected virtual void Start()
        {
                StartServer();
        }

        public virtual void StartServer()
        {
            if (NetworkServer.active)
            {
                Log.Error("Server is ALREADY started");
                return;
            }
            var config = new ConnectionConfig();
            config.AddChannel(QosType.ReliableSequenced);
            config.AddChannel(QosType.Unreliable);
            NetworkServer.Configure(config, 40);

            NetworkServer.Listen(5000);

            //System Operations
            NetworkServer.RegisterHandler(MsgType.Connect, OnClientConnect);
            NetworkServer.RegisterHandler(MsgType.Disconnect, OnClientDisconnect);
            NetworkServer.RegisterHandler(MsgType.Error, OnClientError);

            DontDestroyOnLoad(gameObject);

            ServerStatus = ServerStatus.Running;

            Log.Info(string.Format("Server is started! Listening Port: {0} MaxConnections: {1}", 5000, 40));
        }

        public virtual void StopServer()
        {
            NetworkServer.Shutdown();

            ServerStatus = ServerStatus.Stopped;

            Log.Info("Server shut down!");
        }

        #region System Handlers

        protected virtual void OnClientConnect(NetworkMessage netMsg)
        {
            Debug.Log("A client connected to server. IP: " + netMsg.conn.address);
        }

        protected virtual void OnClientDisconnect(NetworkMessage netMsg)
        {
            Debug.Log("A client disconnected from master");
        }

        protected virtual void OnClientError(NetworkMessage netMsg)
        {
            Debug.Log("Client error received in Master");
        }

        #endregion

        #region Send Commands

        public void Send(Peer peer, short opCode, MessageBase message)
        {
            NetworkServer.SendToClient(peer.ConnectionId, opCode, message);
        }

        public void Send(int connectionId, short opCode, MessageBase message)
        {
            NetworkServer.SendToClient(connectionId, opCode, message);
        }
        
        public void SendToChannel(int channelId, short opCode, MessageBase message)
        {
            NetworkServer.SendByChannelToAll(opCode, message, channelId);
        }

        /// <summary>
        /// Send message to given connectionIds
        /// </summary>
        /// <param name="connectionIds">List of connectionIds</param>
        /// <param name="opCode"></param>
        /// <param name="message"></param>
        public void SendToGivenUsers(List<int> connectionIds, short opCode, MessageBase message)
        {
            foreach (int connectionId in connectionIds)
            {
                Send(connectionId, opCode, message);
            }
        }

        public void SendToAll(short opCode, MessageBase message)
        {
            NetworkServer.SendToAll(opCode, message);
        }

        #endregion

    }
}
