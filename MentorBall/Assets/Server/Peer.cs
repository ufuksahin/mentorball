﻿using System;
using UnityEngine.Networking;

namespace Server
{
    public class Peer : MessageBase, IEquatable<Peer>
    {
        public int ConnectionId;

        public Peer() { }

        public Peer(int connectionId)
        {
            ConnectionId = connectionId;
        }

        public Peer(NetworkReader reader)
        {
            Deserialize(reader);
        }

        public override void Serialize(NetworkWriter writer)
        {
            base.Serialize(writer);

            writer.Write(ConnectionId);
        }

        public override void Deserialize(NetworkReader reader)
        {
            base.Deserialize(reader);

            ConnectionId = reader.ReadInt32();
        }

        #region IEquatable
        public bool Equals(Peer other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ConnectionId == other.ConnectionId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Peer) obj);
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return ConnectionId;
        }
        #endregion
    }
}
