﻿namespace Server
{
    public enum ConnectionStatus
    {
        Disconnected = 0,
        Connected = 2
    }
}
