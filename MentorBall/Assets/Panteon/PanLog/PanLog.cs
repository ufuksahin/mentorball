﻿///////////////////////////////////////////////////////////////
// Copyright - 2014 Panteon
// Project Name: PanLog
// File Name   : PanLog.cs
// Author      : Ufuk Şahin
// Created On  : 15/11/2014 14:00
///////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace Panteon.PanLog
{
    public enum DebugLevels
    {
        None = 0,
        Debug = 5,
        Info = 10,
        Warning = 20,
        Exception = 30,
        Error = 40,
        Special = 50
    }


    public class PanLog
    {
        public static bool IsDebugModeOn = true;
        public static bool WriteTextLogs = true;

        public const string LogFileNameFormat = "Log_{0}.txt";

        private string LogFilePath
        {
            get { return string.Format(LogFileNameFormat, DateTime.UtcNow.ToString("yyyy-MM-dd")); }
        }

        private StreamWriter _logWriter;

        public DebugLevels DebugLevel;
        private string _componentName;

        public string ComponentName
        {
            get
            {
                return (string.IsNullOrEmpty(_componentName) ? string.Empty : string.Format("[{0}] ", _componentName));
            }
            set { _componentName = value; }
        }

        public PanLog(string componentName, DebugLevels debugLevel)
        {
            ComponentName = componentName;
            DebugLevel = debugLevel;

            if (WriteTextLogs)
            {

                if (!File.Exists(LogFilePath))
                {
                    File.CreateText(LogFilePath).Dispose();
                }
            }
        }

        ~PanLog()
        {
            if (WriteTextLogs)
            {
                if (_logWriter != null)
                    _logWriter.Close();
            }
        }

        public void Debug(string log, string prefix = "")
        {
            if (!IsDebugModeOn) return;

            if ((int)DebugLevel <= (int)DebugLevels.Debug)
            {
                ShowLog(log, DebugLevels.Debug, prefix);
            }
        }

        public void Info(string log, string prefix = "")
        {
            if (!IsDebugModeOn) return;

            if ((int)DebugLevel <= (int)DebugLevels.Info)
            {
                ShowLog(log, DebugLevels.Info, prefix);
            }
        }

        public void Warning(string log, string prefix = "")
        {
            if (!IsDebugModeOn) return;

            if ((int)DebugLevel <= (int)DebugLevels.Warning)
            {
                ShowLog(log, DebugLevels.Warning, prefix);
            }
        }

        public void Exception(string log, string prefix = "")
        {
            if (!IsDebugModeOn) return;

            if ((int)DebugLevel <= (int)DebugLevels.Exception)
            {
                ShowLog(log, DebugLevels.Exception, prefix);
            }
        }

        public void Error(string log, string prefix = "")
        {
            if (!IsDebugModeOn) return;

            if ((int)DebugLevel <= (int)DebugLevels.Error)
            {
                ShowLog(log, DebugLevels.Error, prefix);
            }
        }

        public void Special(string log, string prefix = "")
        {
            if (!IsDebugModeOn) return;

            if ((int)DebugLevel == 50)
            {
                ShowLog(log, DebugLevels.Special, prefix);
            }
        }

        private void ShowLog(string log, DebugLevels debugLevel, string prefix)
        {
            if (!string.IsNullOrEmpty(prefix))
                prefix = string.Format("[{0}]", prefix);

            string logContent = string.Format("{0}{1}: {2}", ComponentName, prefix, log);

            switch (debugLevel)
            {
                case DebugLevels.Debug:
                    UnityEngine.Debug.Log("[DEBUG] " + logContent);
                    break;
                case DebugLevels.Info:
                    UnityEngine.Debug.Log(logContent);
                    break;
                case DebugLevels.Warning:
                    UnityEngine.Debug.LogWarning(logContent);
                    break;
                case DebugLevels.Exception:
                    UnityEngine.Debug.LogException(new Exception(logContent));
                    break;
                case DebugLevels.Error:
                    UnityEngine.Debug.LogError(logContent);
                    break;
                case DebugLevels.Special:
                    UnityEngine.Debug.LogError(logContent);
                    break;
            }

            if (WriteTextLogs)
            {
                _logWriter = File.AppendText(LogFilePath);
                _logWriter.WriteLine("{0} - {1}", DateTime.Now, log);
                _logWriter.Flush();
                _logWriter.Close();
            }
        }
    }
}