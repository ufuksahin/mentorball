﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreItem : MonoBehaviour
{
    public Text Name;
    public Text Score;

    public void Initialize(string name, int score)
    {
        Name.text = name;
        Score.text = score.ToString();
    }
}
