﻿using System;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public static class ServerBuilder
    {
        /// <summary>
        /// Have in mind that if you change it, it might take "a while" 
        /// for the editor to pick up changes 
        /// </summary>
        public static string Root = "Assets";

        public static BuildTarget TargetPlatform = BuildTarget.StandaloneWindows;

        /// <summary>
        /// Build with "Development" flag, so that we can see the console if something 
        /// goes wrong
        /// </summary>
        public static BuildOptions BuildOptions = BuildOptions.Development;

        public static string PrevPath = null;

        [MenuItem("Tools/Build Game Server", false, 0)]
        public static void BuildGameServer()
        {
            var path = GetPath();
            if (string.IsNullOrEmpty(path))
                return;

            BuildGameServer(path + "/Server/Server.exe");
            EditorUtility.RevealInFinder(path);
        }
        
        [MenuItem("Tools/Build Client", false, 0)]
        public static void BuildClient()
        {
            var path = GetPath();
            if (string.IsNullOrEmpty(path))
                return;

            BuildGameServer(path + "/Client/Client.exe");
            EditorUtility.RevealInFinder(path);
        }
        
        /// <summary>
        /// Creates a build for gane server
        /// </summary>
        /// <param name="path"></param>
        public static void BuildGameServer(string path)
        {
            var scenes = new[]
            {
                Root+"/Scenes/Server.unity",
            };

            Debug.LogFormat("Added {0} into scenes.", scenes[0]);

            PlayerSettings.forceSingleInstance = false;

            Debug.Log("Building Game Server into " + path);

            BuildPipeline.BuildPlayer(scenes, path, TargetPlatform, BuildOptions);
        }

        /// <summary>
        /// Creates a build for gane server
        /// </summary>
        /// <param name="path"></param>
        public static void BuildClient(string path)
        {
            var scenes = new[]
            {
                Root+"/Scenes/Client.unity",
            };

            Debug.LogFormat("Added {0} into scenes.", scenes[0]);

            PlayerSettings.forceSingleInstance = false;

            Debug.Log("Building Client into " + path);

            BuildPipeline.BuildPlayer(scenes, path, TargetPlatform, BuildOptions);
        }

        public static string GetPath()
        {
            string[] args = Environment.GetCommandLineArgs();
            string buidPathInput = "";
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-buildPath")
                {
                    buidPathInput = args[i + 1];
                    Debug.Log("Found Path Command -> " + buidPathInput);
                }
            }

            if (!string.IsNullOrEmpty(buidPathInput))
                return buidPathInput;

            var prevPath = EditorPrefs.GetString("srv.buildPath", "");
            string path = EditorUtility.SaveFolderPanel("Choose Location for binaries", prevPath, "");

            if (!string.IsNullOrEmpty(path))
            {
                EditorPrefs.SetString("srv.buildPath", path);
            }
            return path;
        }
    }
}